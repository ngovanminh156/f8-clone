import React, { memo } from "react";
import "./Learning.scss";
import FreeCourse from "../../../component/Organisms/FreeCourse/FreeCourse";
import Header from "../../Organisms/Header/Header";
import Sidebar from "../../Organisms/Sidebar/Sidebar";
import Footer from "../../Organisms/Footer/Footer";

const Learning = () => {
  return (
    <div className="learning-wrapper">
      <Header />
      <div  style={{ display: "flex" }}>
        <Sidebar />
        <div className="learning__info">
          <div className="learning__intro">
            <h1>Khóa học</h1>
            <p>
              Các khóa học được thiết kế phù hợp cho cả người mới, nhiều khóa
              học miễn phí, chất lượng, nội dung dễ hiểu.
            </p>
          </div>
          <FreeCourse />
          <div className="learning__community">
            <div>
              <h1>Bạn đang tìm kiếm lộ trình học cho người mới?</h1>
              <p>
                Các khóa học được thiết kế phù hợp cho người mới, lộ trình học
                rõ ràng, nội dung dễ hiểu.
              </p>
              <button>
                <a href="/learning-path">Xem lộ trình</a>
              </button>
            </div>
            <div>
              <img
                src="https://fullstack.edu.vn/static/media/fb-group-cards.4bd525b1b8baf7b1e5a2.png"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default memo(Learning);
