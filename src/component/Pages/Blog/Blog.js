import React, { memo } from "react";
import "./Blog.scss";
import Header from "../../Organisms/Header/Header";
import Sidebar from "../../Organisms/Sidebar/Sidebar";
import Footer from "../../Organisms/Footer/Footer";

const Blog = () => {
  return (
    <div className="blog-wrapper">
      <Header />
      <div style={{ display: "flex" }} className="blog-container">
        <Sidebar />
        <div className="blog-content">
          <div className="blog-content-heading">
            <h1>Bài viết nổi bật</h1>
            <p>
              Tổng hợp các bài viết chia sẻ về kinh nghiệm tự học lập trình
              online và các kỹ thuật lập trình web.
            </p>
          </div>
          <div className="blog-content-post">
            <div>
              <div className="PostItem_wrapper__5s6Lk">
              <div class="PostItem_header__kJhep">
              <div class="PostItem_author__-CiNM">
              <a href="/@khang-ng">
              <div class="FallbackAvatar_avatar__gmj3S FallbackAvatar_pro__-8mK+" style="--font-size: 2.9px;">
              <img src="https://files.fullstack.edu.vn/f8-prod/user_avatars/18810/631175d26916f.png" alt="Dev Quèn">
              <img class="FallbackAvatar_crown__BnONf" src="/static/media/crown.8edf462029b3c37a7f673303d8d3bedc.svg" alt="crown">

              </div>
              </a>
              <a href="/@khang-ng">
              <span>Dev Quèn</span>
              </a>
              </div>
              <div class="PostItem_actions__eWV5m">
              <div class="Bookmark_toggleBtn__sSS-2 PostItem_optionBtn__h8ALw">
              <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="bookmark" class="svg-inline--fa fa-bookmark " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
              <path fill="currentColor" d="M336 0h-288C21.49 0 0 21.49 0 48v431.9c0 24.7 26.79 40.08 48.12 27.64L192 423.6l143.9 83.93C357.2 519.1 384 504.6 384 479.9V48C384 21.49 362.5 0 336 0zM336 452L192 368l-144 84V54C48 50.63 50.63 48 53.1 48h276C333.4 48 336 50.63 336 54V452z">

              </path>
              </svg>
              </div>
              <div class="PostItem_optionBtn__h8ALw" aria-expanded="false">
              <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="ellipsis" class="svg-inline--fa fa-ellipsis " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
              <path fill="currentColor" d="M120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200C94.93 200 120 225.1 120 256zM280 256C280 286.9 254.9 312 224 312C193.1 312 168 286.9 168 256C168 225.1 193.1 200 224 200C254.9 200 280 225.1 280 256zM328 256C328 225.1 353.1 200 384 200C414.9 200 440 225.1 440 256C440 286.9 414.9 312 384 312C353.1 312 328 286.9 328 256z">

              </path>
              </svg>
              </div>
              </div>
              </div>
              <div class="PostItem_body__Fnfo-">
              <div class="PostItem_info__DZr39">
              <a href="/blog/tat-tan-tat-ve-cai-thien-performance-cua-1-trang-web.html">
              <h2 class="PostItem_title__8lSHm">`Tất tần tật` về cải thiện Performance của 1 trang web🚀</h2>
              </a>
              <p class="PostItem_desc__be9G8">Ở bài viết này, chúng ta cùng nhau tìm hiểu về các vấn đề liên quan đến Performance ở phía FE. Không dài dòng nữa,...</p>
              <div class="PostItem_info__DZr39">
              <a class="PostItem_tags__0M1lV" href="/blog/tag/javascript?page=1">Javascript
              </a>
              <span>một ngày trước</span>
              <span class="PostItem_dot__ZYdFt">·
              </span>8 phút đọc
              </div>
              </div><div class="PostItem_thumb__m4iXl">
              <a href="/blog/tat-tan-tat-ve-cai-thien-performance-cua-1-trang-web.html">
              <img src="https://files.fullstack.edu.vn/f8-prod/blog_posts/7940/64a645ea70312.png" alt="`Tất tần tật` về cải thiện Performance của 1 trang web🚀">

              </a>
              </div>
              </div>
              </div>
            </div>
            <div>

            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Blog;
