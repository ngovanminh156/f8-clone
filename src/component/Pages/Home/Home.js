import Header from "../../Organisms/Header/Header.js";
import Sidebar from "../../Organisms/Sidebar/Sidebar.js";
import Slider from "../../Organisms/Slider/Slider.js";
import FreeCourse from "../../Organisms/FreeCourse/FreeCourse.js";
import Post from "../../Organisms/Post/Post.js";
import Footer from "../../Organisms/Footer/Footer.js";
import Video from "../../Organisms/Video/Video.js";

function Home() {
  return (
    <div className="container">
      <div>
        <Header />
      </div>
      <div
        style={{
          display: "flex",
          // height: "100%"
          marginTop: "100px",
        }}
      >
          <Sidebar />

        <div
          style={{
            width: "85%",
            flexDirection: "column",
            padding: "0px 0px 74px 30px",
          }}
        >
          <Slider />
          <div
            style={{
              marginTop: "70px",
              // justifyContent: "center",
              padding: "0 20px",
            }}
          >
            <FreeCourse />
            <Post />
            <Video />
          </div>
        </div>
      </div>

      <div>
        <Footer />
      </div>
    </div>
  );
}

export default Home;
