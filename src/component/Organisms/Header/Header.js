import f8image from "../../../img/f8-logo.png";
import "./Header.scss";

function Header() {
  return (
    <div className="navbar-wrapper">
      <div className="navbar-logo">
        <a href="/">
          <img src={f8image} alt="F8" />
        </a>
        <h4>Học Lập Trình Để Đi Làm</h4>
      </div>
      <div class="MobileMenu_mobileMenu">
        <div class="MobileMenu_wrapper__jgA7H">
          <em>
            <svg
              aria-hidden="true"
              focusable="false"
              data-prefix="fas"
              data-icon="bars"
              class="svg-inline--fa fa-bars "
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 448 512"
            >
              <path
                fill="currentColor"
                d="M0 96C0 78.33 14.33 64 32 64H416C433.7 64 448 78.33 448 96C448 113.7 433.7 128 416 128H32C14.33 128 0 113.7 0 96zM0 256C0 238.3 14.33 224 32 224H416C433.7 224 448 238.3 448 256C448 273.7 433.7 288 416 288H32C14.33 288 0 273.7 0 256zM416 448H32C14.33 448 0 433.7 0 416C0 398.3 14.33 384 32 384H416C433.7 384 448 398.3 448 416C448 433.7 433.7 448 416 448z"
              ></path>
            </svg>
          </em>
        </div>
        <div></div>
      </div>
      <div className="navbar-search">
        <div className="navbar-search-input">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            class="bi bi-search"
            viewBox="0 0 16 16"
          >
            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
          </svg>
          <input
            type="text"
            className="search-input"
            spellcheck="false"
            placeholder="Tìm kiếm khóa học, bài viết, video, ..."
            // value=""
          ></input>
        </div>
      </div>
      <div className="navbar-login-button">
      <a class="navbar-search-btn" href="/search">
        <svg
          aria-hidden="true"
          focusable="false"
          data-prefix="fas"
          data-icon="magnifying-glass"
          class="svg-inline--fa fa-magnifying-glass NavBar_action-icon__l9MxX"
          role="img"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 512 512"
        >
          <path
            fill="currentColor"
            d="M500.3 443.7l-119.7-119.7c27.22-40.41 40.65-90.9 33.46-144.7C401.8 87.79 326.8 13.32 235.2 1.723C99.01-15.51-15.51 99.01 1.724 235.2c11.6 91.64 86.08 166.7 177.6 178.9c53.8 7.189 104.3-6.236 144.7-33.46l119.7 119.7c15.62 15.62 40.95 15.62 56.57 0C515.9 484.7 515.9 459.3 500.3 443.7zM79.1 208c0-70.58 57.42-128 128-128s128 57.42 128 128c0 70.58-57.42 128-128 128S79.1 278.6 79.1 208z"
          ></path>
        </svg>
      </a>
        <a className="login-button" href="/login">
          Đăng nhập
        </a>
      </div>
    </div>
  );
}

export default Header;
