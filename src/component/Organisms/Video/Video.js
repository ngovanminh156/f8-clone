import React, { memo } from "react";
import {
  RightOutlined,
  CaretRightOutlined,
  EyeOutlined,
  LikeOutlined,
  MessageOutlined,
} from "@ant-design/icons";
import { Button } from "antd";
import Videoss from "../../../api/video.json";
import "./Video.scss";

const Video = () => {
  return (
    <div className="videos" >
      <div className="videos__title">
        <a href="/">Video nổi bật</a>
        <a href="/">
          Xem tất cả <RightOutlined />
        </a>
      </div>
      
      <div className="videos__list">
          {Videoss.map((video) => (
          
              <div className="videos__list__items">
                <a href="/">

                  <div className="videos__list__items__img">
                    <img src={video.image} alt="" />
                    <div className="videos__list__items__btn">
                      <Button shape="round">Xem Video</Button>
                    </div>
                    <div className="videos__list__items_icon">
                      <div>
                        <CaretRightOutlined />
                      </div>
                      <button>{video.time}</button>
                    </div>
                  </div>

                  <h3>{video.title}</h3>

                  <div className="videos__list__items__footer">
                    <span>
                      <EyeOutlined /> {video.view}
                    </span>
                    <span>
                      <LikeOutlined /> {video.like}
                    </span>
                    <span>
                      <MessageOutlined /> {video.mess}
                    </span>
                  </div>
                </a>
              </div>
          ))}
      </div>
    </div>
  );
};

export default memo(Video);
