import blogs from "../../../api/blog.json";
import React from "react";
import "./Post.scss";

function Post() {
  return (
    <div className="blog">
      <div className="blog-heading">
        <div className="blog-heading-title">
          <h2>
            <a rel="noreferrer" target="_self" href="/learning-paths">
              Bài viết nổi bật
            </a>
          </h2>
        </div>

        <div className="blog-heading-view-all">
          <a
            class="ScrollList"
            rel="noreferrer"
            target="_self"
            href="/learning-paths"
          >
            Xem tất cả
            <svg
              aria-hidden="true"
              focusable="false"
              data-prefix="fas"
              data-icon="chevron-right"
              class="svg-inline--fa fa-chevron-right "
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 320 512"
            >
              <path
                fill="currentColor"
                d="M96 480c-8.188 0-16.38-3.125-22.62-9.375c-12.5-12.5-12.5-32.75 0-45.25L242.8 256L73.38 86.63c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0l192 192c12.5 12.5 12.5 32.75 0 45.25l-192 192C112.4 476.9 104.2 480 96 480z"
              ></path>
            </svg>
          </a>
        </div>
      </div>

      <div className="blog-container">
        {blogs.map((blog) => (
          <div key={blog.title} className="blog-container-item">
            <a href="/" >
              <div className="item-image">
                <img src={blog.image} alt={blog.title} />
                <div>
                  <button className="hoverBtn" onClick={""}>
                    Xem bài viết
                  </button>
              </div>
              </div>
            </a>
            <div className="item-title">
              <h3>{blog.title}</h3>
            </div>
            <div className="item-author">
              <img src={blog.auth_logo} alt={""} />
              <span>{blog.auth_name}</span>
              <li>{blog.read_time}</li>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Post;
