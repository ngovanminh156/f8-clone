import React from "react";
import courses from "../../../api/freeCourses.json";
import "./FreeCourse.scss";

function FreeCourse() {
  return (
    <div className="freeCourse">
      <div className="heading">
        <div className="heading-total-student">
          <p>
            <strong>328.220+ </strong>
            người khác đã học
          </p>
        </div>
        <div className="heading-title">
          <div className="heading-title-name">
          <h2>
            <a rel="noreferrer" target="_self" href="/learning-paths">
              Khóa học miễn phí
            </a>
          </h2>
        </div>
        <div className="heading-view-all">
          <a
            class="ScrollList"
            rel="noreferrer"
            target="_self"
            href="/learning-paths"
          >
            Xem lộ trình
            <svg
              aria-hidden="true"
              focusable="false"
              data-prefix="fas"
              data-icon="chevron-right"
              class="svg-inline--fa fa-chevron-right "
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 320 512"
            >
              <path
                fill="currentColor"
                d="M96 480c-8.188 0-16.38-3.125-22.62-9.375c-12.5-12.5-12.5-32.75 0-45.25L242.8 256L73.38 86.63c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0l192 192c12.5 12.5 12.5 32.75 0 45.25l-192 192C112.4 476.9 104.2 480 96 480z"
              ></path>
            </svg>
          </a>
        </div>
      </div>
      </div>

      <div className="freeCourse-container">
        {courses.map((course) => (
          <div key={course.title} className="freeCourse-item">
            <a href="/" className="freeCourse-item-image">
              <img src={course.image} alt={course.title} />
              <div>
                <button className="hoverBtn" onClick={""}>
                  Xem khóa học
                </button>
            </div>
            </a>
            <div>
              <h3>{course.title}</h3>
            </div>
            <div className="freeCourse-item-student">
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  data-icon="users"
                  class="svg-inline--fa fa-users "
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 640 512"
                >
                  <path
                    fill="currentColor"
                    d="M319.9 320c57.41 0 103.1-46.56 103.1-104c0-57.44-46.54-104-103.1-104c-57.41 0-103.1 46.56-103.1 104C215.9 273.4 262.5 320 319.9 320zM369.9 352H270.1C191.6 352 128 411.7 128 485.3C128 500.1 140.7 512 156.4 512h327.2C499.3 512 512 500.1 512 485.3C512 411.7 448.4 352 369.9 352zM512 160c44.18 0 80-35.82 80-80S556.2 0 512 0c-44.18 0-80 35.82-80 80S467.8 160 512 160zM183.9 216c0-5.449 .9824-10.63 1.609-15.91C174.6 194.1 162.6 192 149.9 192H88.08C39.44 192 0 233.8 0 285.3C0 295.6 7.887 304 17.62 304h199.5C196.7 280.2 183.9 249.7 183.9 216zM128 160c44.18 0 80-35.82 80-80S172.2 0 128 0C83.82 0 48 35.82 48 80S83.82 160 128 160zM551.9 192h-61.84c-12.8 0-24.88 3.037-35.86 8.24C454.8 205.5 455.8 210.6 455.8 216c0 33.71-12.78 64.21-33.16 88h199.7C632.1 304 640 295.6 640 285.3C640 233.8 600.6 192 551.9 192z"
                  ></path>
                </svg>
                <span>{course.register}</span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
export default FreeCourse;
