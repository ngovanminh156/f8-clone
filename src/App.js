import {
  BrowserRouter as Router,
  Route,
  Routes,
} from "react-router-dom";
import Home from "./component/Pages/Home/Home";
import Login from "./component/Pages/Login/Login";
import Register from "./component/Pages/Register/Register";
import LearningPath from "./component/Pages/LearningPath/LearningPath";
import Learning from "./component/Pages/Learning/Learning";
import Blog from './component/Pages/Blog/Blog';


function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/learning-path" element={<LearningPath />} />
        <Route path="/learning" element={<Learning />} />
        <Route path="/blog" element={<Blog />} />
      </Routes>
    </Router>
  );
}

export default App;
